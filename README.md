# flipswitch

Write your configuration file for create scenario what will be happen when you switching to tablet mode and laptop

### Install
Clone be git repository or download archive.
Enter in project folder and execute following command:
```
chmod +x install.sh
./install.sh
```
### Uninstall
For uninstall execute:
```
chmod +x uninstall.sh
./uninstall.sh
```
### Execute
After installing you will be find the "Tablet mode" and "Laptop mode" programms in you applications menu.
Whatever you will be switch modes from terminal:
```
flipswitch laptop
```
or switch to tablet mode
```
flipswitch tablet
```

### Creating config file

Config file wold be located in ~/.config/ and named as flipswitch.json
Example config file:
```
{
    "laptop": {
        "disable": [],
        "enable": [
            "AT Translated Set 2 keyboard", # enable keyboard
            "ELAN1401:00 04F3:30DC Touchpad" # enable touchpad
        ],
        "actions": [
            "echo 1 | sudo tee '/sys/class/leds/asus::kbd_backlight/brightness'" # enable keyboard brightness with 1 levl
        ]
    },
    "tablet": {
        "disable": [
            "AT Translated Set 2 keyboard", # disable keyboard
            "ELAN1401:00 04F3:30DC Touchpad" # disable touchpad
        ],
        "enable": [],
        "actions": [
            "echo 0 | sudo tee '/sys/class/leds/asus::kbd_backlight/brightness'" # disable keyboard brightness
        ]
    }
}
```

#### "laptop" and "tablet" sections
This sections will be processing when you execute in terminal ```flipswitch tablet # or laptop```
or call programm "Tablet mode" or "Laptop mode" in your applications menu.

#### "disable" and "enable" sections
This section contain devices witch are you need disable or enable like as keyboard, touchpad etc.
For watching all devices call in terminal ```xinput```, for me this command output this:
```
 Virtual core pointer                          id=2    [master pointer  (3)]
⎜   ↳ Virtual core XTEST pointer                id=4    [slave  pointer  (2)]
⎜   ↳ ELAN1401:00 04F3:30DC Mouse               id=12   [slave  pointer  (2)]
⎜   ↳ ELAN9008:00 04F3:290F                     id=15   [slave  pointer  (2)]
⎜   ↳ ELAN1401:00 04F3:30DC Touchpad            id=13   [slave  pointer  (2)]
⎣ Virtual core keyboard                         id=3    [master keyboard (2)]
    ↳ Virtual core XTEST keyboard               id=5    [slave  keyboard (3)]
    ↳ Power Button                              id=6    [slave  keyboard (3)]
    ↳ Video Bus                                 id=7    [slave  keyboard (3)]
    ↳ Power Button                              id=8    [slave  keyboard (3)]
    ↳ Sleep Button                              id=9    [slave  keyboard (3)]
    ↳ USB2.0 HD IR UVC WebCam: USB2.0           id=10   [slave  keyboard (3)]
    ↳ USB2.0 HD IR UVC WebCam: USB2.0           id=11   [slave  keyboard (3)]
    ↳ ELAN1401:00 04F3:30DC Keyboard            id=14   [slave  keyboard (3)]
    ↳ ELAN9008:00 04F3:290F                     id=16   [slave  keyboard (3)]
    ↳ Asus WMI hotkeys                          id=17   [slave  keyboard (3)]
    ↳ ACPI Virtual Keyboard Device              id=19   [slave  keyboard (3)]
    ↳ AT Translated Set 2 keyboard              id=18   [slave  keyboard (3)]
```
**See [config file](./flipswitch.json) for example.**

#### "actions" section
Contain the cmd commands like as .sh, .py, or other what would be executed in cmd
for me is a command for disable keyboard brightness.